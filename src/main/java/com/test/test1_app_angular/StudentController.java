package com.test.test1_app_angular;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping({"/student"})
public class StudentController {
    
    @Autowired
    StudentService service;
    
    @GetMapping
    public List<Student>listShow(){
        return service.listShow();
    }
    
    @PostMapping
    public Student add(@RequestBody Student p){
       return service.add(p);
    }
    
    @GetMapping(path = {"/{id}"})
    public Student listId(@PathVariable("id")int id){
        return service.listId(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Student edit(@RequestBody Student p, @PathVariable("id")int id ){
        p.setId(id);
        return service.edit(p);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Student delete(@PathVariable("id")int id ){
        return service.delete(id);
    }
}
