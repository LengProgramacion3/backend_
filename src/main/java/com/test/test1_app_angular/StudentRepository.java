package com.test.test1_app_angular;

import java.util.List;
import org.springframework.data.repository.Repository;

/**
 *
 * @author haas
 */
public interface StudentRepository extends Repository<Student, Integer>{
    List<Student>findAll();
    Student findByid(int id);
    Student save(Student p);
    void delete(Student p);

}
