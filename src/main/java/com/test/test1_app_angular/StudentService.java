package com.test.test1_app_angular;

import java.util.List;

/**
 *
 * @author haas
 */
public interface StudentService {
    List<Student>listShow();
    Student listId(int id);
    Student add(Student p);
    Student edit(Student p);
    Student delete(int id);
}
