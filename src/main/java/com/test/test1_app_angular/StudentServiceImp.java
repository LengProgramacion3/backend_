package com.test.test1_app_angular;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author haas
 */
@Service
public class StudentServiceImp implements StudentService{

    @Autowired
    private StudentRepository r;
    @Override
    public List<Student> listShow() {
       return r.findAll();
    }

    @Override
    public Student listId(int id) {
        return r.findByid(id);
    }

    @Override
    public Student add(Student p) {
        return r.save(p);
    }

    @Override
    public Student edit(Student p) {
        return r.save(p);
    }

    @Override
    public Student delete(int id) {
        Student p = r.findByid(id);
        if(p!=null){
            r.delete(p);            
        }
        return p;

    }
    
}
