package com.test.test1_app_angular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test1AppAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(Test1AppAngularApplication.class, args);
	}

}
